import 'package:flutter/material.dart';
import 'dart:async';
import 'dart:convert';
import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'brands.dart';
import 'listposts.dart';

Future<List<Brand>> fetchPosts(http.Client client) async {
  final response = await client.post(
      'https://api-dev.fiyuu.com.tr/customer/product/scanBrands-1.0',
      headers: {'token':'XpcLEEIMqSuTiDlCkww0swM4i4b2sv+UE3m2XupA3mHeww5mnn9DpAhcaaraLuAOoyO0i+3K8upOmZxJoWQVI0ABAv6slC+/bOtFbJfLWZy3huWYjOw6aLmwz45H8qBc/Vlhcr+czKU/7H0vR5zwIa+ZUw=='
      });
  print(response.body);

  return compute(parsePosts, response.body);
}

List<Brand> parsePosts(String responseBody) {
  final js = json.decode(responseBody);
  final parsed = js['data'].cast<Map<String, dynamic>>();

  return parsed.map<Brand>((json) => Brand.fromJson(json)).toList();
}

class HomePage extends StatelessWidget {
  final String title;

  HomePage({Key key, this.title}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: Colors.pinkAccent,
        title: Text(title),
      ),
      body: FutureBuilder<List<Brand>>(
        future: fetchPosts(http.Client()),
        builder: (context, snapshot) {
          if (snapshot.hasError) print(snapshot.error);

          return snapshot.hasData
              ? ListViewPosts(brands: snapshot.data)
              : Center(child: CircularProgressIndicator());
        },
      ),
    );
  }
}