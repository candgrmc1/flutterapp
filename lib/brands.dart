class Brand {
  final String id;
  final String name;
  final String image;

  Brand({this.id, this.name,this.image});

  factory Brand.fromJson(Map<String, dynamic> json) {
    return Brand(
      id: json['id'] as String,
      name: json['name'] as String,
      image: json['image'] as String,
    );
  }
}
