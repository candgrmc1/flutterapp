import 'package:flutter/material.dart';
import 'brands.dart';

class ListViewPosts extends StatelessWidget {
  final List<Brand> brands;

  ListViewPosts({Key key, this.brands}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: ListView.builder(
          itemCount: brands.length,
          padding: const EdgeInsets.all(15.0),
          itemBuilder: (context, position) {
            return Column(
              children: <Widget>[
                Divider(height: 5.0),
                ListTile(
                  title: Text(
                    '${brands[position].name}',
                    style: TextStyle(
                      fontSize: 22.0,
                      color: Colors.black38,
                    ),
                  ),

                  leading: Column(
                    children: <Widget>[
                      CircleAvatar(
                        child: Image.network('https://bkmexpress.com.tr/wp-content/uploads/2018/04/fiyuu.jpg')
                      )
                    ],
                  ),
                  onTap: () => _onTapItem(context, brands[position]),
                ),
              ],
            );
          }),
    );
  }

  void _onTapItem(BuildContext context, Brand brand) {
    Scaffold
        .of(context)
        .showSnackBar(new SnackBar(content: new Text(brand.id.toString())));


  }
}
